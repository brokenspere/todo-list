import React from "react";
import TodoContext from "./Context";
import "./App.css";
import Router from "./Router";

function App() {
  return (
    <TodoContext>
      <Router />
    </TodoContext>
  );
}

export default App;
