import React, { useState } from "react";
export const TodoContext = React.createContext();

const Context = ({ children }) => {
  const [list, setList] = useState([]);
  const [history, setHistory] = useState([]);

  const handleOk = (todo) => {
    setList([...list, todo]);
  };

  const deleteTask = (item) => {
    setList((prevList) => prevList.filter((i) => i !== item));
  };

  const handleEdit = (edit, index) => {
    const newTodos = [...list];
    newTodos[index].title = edit;
    setList(newTodos);
  };

  const handleDone = (index) => {
    const updatedItems = list.map((item, indx) =>
      index === indx ? { ...item, isDone: true } : item
    );
    setList(updatedItems);
  };

  const addHistory = (data) => {
    setHistory([...history, data]);
  };

  const cleatHistory = () => {
    setHistory([]);
  };

  return (
    <TodoContext.Provider
      value={{
        items: list,
        historyData: history,
        addHistory: addHistory,
        handleAdd: handleOk,
        handleDelete: deleteTask,
        EditTodo: handleEdit,
        handleDone: handleDone,
        clear: cleatHistory,
      }}
    >
      {children}
    </TodoContext.Provider>
  );
};
export default Context;
