import React, { useContext, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { TodoContext } from "../../Context";
import { List, Button, Modal, Input } from "antd";
import { Form, Field } from "react-final-form";
import { CheckCircleOutlined } from "@ant-design/icons";
import moment from "moment";

const Home = () => {
  const { Search } = Input;
  const [todo, setTodo] = useState({});
  const {
    items,
    addHistory,
    handleAdd,
    handleDelete,
    EditTodo,
    handleDone,
  } = useContext(TodoContext);
  const [init, setInit] = useState({});
  const [visible, setVisible] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [data, setData] = useState([]);
  const history = useHistory();

  useEffect(() => {
    setData(items);
  }, [items]);

  const handleShow = () => {
    setVisible(true);
  };

  const onEdit = (item, index) => {
    setInit({ todo: item.title, index });
    setVisible(true);
  };

  const handleChange = (event) => {
    setTodo(event.target.value);
  };

  const handleFinish = (item, index) => {
    let history = {
      date: moment().format("MMMM Do YYYY, h:mm:ss"),
      type: "Finish todo",
      detail: item.title,
    };
    handleDone(index);
    addHistory(history);
  };

  const sumbitFm = (values) => {
    const { todo, index } = values;
    if (Object.values(init) > -1) {
      let data = {
        title: todo,
        isDone: false,
      };
      let history = {
        date: moment().format("MMMM Do YYYY, h:mm:ss"),
        type: "Create todo",
        detail: todo,
      };
      handleAdd(data);
      addHistory(history);
      handleCancel();
    } else {
      let history = {
        date: moment().format("MMMM Do YYYY, h:mm:ss"),
        type: "Edit todo",
        detail: todo,
      };
      EditTodo(todo, index);
      addHistory(history);
      handleCancel();
    }
  };

  const deleteTodo = (item) => {
    let history = {
      date: moment().format("MMMM Do YYYY, h:mm:ss"),
      type: "Delete todo",
      detail: item.title,
    };
    handleDelete(item);
    addHistory(history);
  };

  const handleCancel = () => {
    setVisible(false);
    setInit({});
  };

  const handleSearch = (e) => {
    setSearchValue(e.target.value);
    if (searchValue) {
      setTimeout(() => {
        const filterItem = data.filter((item) => {
          return item.title.toLowerCase().includes(searchValue.toLowerCase());
        });
        setData(filterItem);
      }, 500);
    }
  };

  return (
    <div
      style={{
        alignItems: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        padding: 15,
      }}
    >
      <div>
        <text>Home page</text>
        <span style={{ padding: 5 }}>
          <Search
            placeholder="input search todo"
            style={{ width: 200 }}
            onChange={(e) => handleSearch(e)}
          />
        </span>
      </div>
      <div style={{ marginTop: 20 }}>
        <List
          bordered
          dataSource={data}
          renderItem={(item, index) => (
            <List.Item
              actions={[
                <a
                  key="list-loadmore-edit"
                  onClick={() => handleFinish(item, index)}
                >
                  Done
                </a>,
                <a key="list-loadmore-edit" onClick={() => onEdit(item, index)}>
                  Edit
                </a>,
                <a key="list-loadmore-edit" onClick={() => deleteTodo(item)}>
                  Delete
                </a>,
              ]}
            >
              <div>
                {item.isDone == true ? (
                  <CheckCircleOutlined
                    style={{ color: "green", marginRight: 10 }}
                  />
                ) : null}
                {item.title}
              </div>
            </List.Item>
          )}
        />
      </div>
      <div style={{ marginTop: 20 }}>
        <Button type="primary" onClick={handleShow}>
          Add todo
        </Button>
        <Button
          style={{ marginLeft: 10 }}
          type="primary"
          onClick={() => history.push("/history")}
        >
          History
        </Button>
      </div>
      <Modal
        title="New Todo"
        visible={visible}
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          onSubmit={sumbitFm}
          initialValues={init}
          validate={(values) => {
            const error = {};
            if (!values.todo) {
              error.todo = "Enter Task";
            }
            return error;
          }}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit} id="myForm">
              <div>
                <label>Add new task</label>
                <Field
                  name="todo"
                  component="input"
                  type="text"
                  placeholder="Enter todo"
                  onChange={handleChange}
                >
                  {({ input, placeholder, meta }) => (
                    <div>
                      <input
                        className="form-input"
                        {...input}
                        placeholder={placeholder}
                      />
                      {meta.error && meta.touched && <span>{meta.error}</span>}
                    </div>
                  )}
                </Field>
              </div>
              <div style={{ paddingTop: 10 }}>
                <button type="submit">submit</button>
              </div>
            </form>
          )}
        />
      </Modal>
    </div>
  );
};
export default Home;
