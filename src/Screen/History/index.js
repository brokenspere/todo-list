import React, { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { TodoContext } from "../../Context";
import { List, Button } from "antd";
import _ from "lodash";

const History = () => {
  const { historyData, clear } = useContext(TodoContext);
  const [data, setData] = useState([]);
  const history = useHistory();

  useEffect(() => {
    var data = _.orderBy(historyData, ["date"], ["desc"]);
    setData(data);
  }, []);

  const clearData = () => {
    setData([]);
    clear();
  };

  return (
    <div
      style={{
        alignItems: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        padding: 15,
      }}
    >
      <text>History page</text>
      <div style={{ marginTop: 20 }}>
        <List
          bordered
          dataSource={data}
          renderItem={(item, index) => (
            <List.Item>
              <div>
                <span style={{ padding: 5 }}>{item.date} |</span>
                <span style={{ padding: 5 }}>{item.type} |</span>
                <span style={{ padding: 5 }}>{item.detail} </span>
              </div>
            </List.Item>
          )}
        />
      </div>
      <Button
        style={{ marginLeft: 10, marginTop: 20 }}
        type="primary"
        onClick={() => history.push("/")}
      >
        Home
      </Button>
      <Button
        style={{ marginLeft: 10, marginTop: 20 }}
        type="primary"
        onClick={() => clearData()}
      >
        Clear history
      </Button>
    </div>
  );
};
export default History;
