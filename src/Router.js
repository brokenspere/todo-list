import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./Screen/Home";
import History from "./Screen/History";

const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/history" component={History} />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
